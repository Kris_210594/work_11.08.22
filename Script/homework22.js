"use strict";


// Упражнение 1
let arr = ["a", {}, 1, 28, "Kris", -3];

function getSumm(arr) {
    let newArr = arr.filter(n => typeof n === 'number');
    let sum = 0;

    for (let i = 0; i < newArr.length; i++)
        sum += newArr[i];
    return sum;
}

console.log(getSumm(arr));

// Упражнение 3
// В корзине один товар
let cart = [4884];

function addToCart(productId) {
    let hasInCart = cart.includes(productId);

    if (hasInCart) return;

    cart.push(productId);
}

function removeFromCart(productId) {
    cart = cart.filter(function (id) {
        return id !== productId;

    })
}

// Добавили товар
addToCart(3456);
// Повторно добавили товар
addToCart(3456);
console.log('', cart); // [4884, 3456]

// Удалили товар
removeFromCart(4884);
console.log('', cart); //  [3456]


