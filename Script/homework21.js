"use strict";


// Упражнение 1
const object = {
    // userName: 'John',
};

/**
 * Check any object
 * @param {object} nameObeject
 * @param {string} key
 * @returns true if object is empty
 */
function isEmpty(nameObeject) {
    for (let key in nameObeject) {
        return false;
    }
    return true;
}

console.log(isEmpty(object));

// Упражнение 3
let salaries = {
    John: 100000,
    Ann: 160000,
    Pete: 130000,
};

function raiseSalary(perzent) {
    let newSalaries = {};

    for (let key in salaries) {

        let raise = (salaries[key] * perzent) / 100;

        newSalaries[key] = salaries[key] + raise;
    }

    return newSalaries;

}
let result = raiseSalary(5);

console.log(salaries, result);