"use strict";


// Упражнение 1
for (let i = 1; i <= 20; i++) {
    if (i % 2 == 0)
        alert(i);
}

// Упражнение 2

let sum = 0;
//Решение
for (let i = 0; i < 3; i++) {
    let number = +prompt("Введите число");

    console.log(number);

    if (isNaN(number)) {
        alert("Ошибка. Вы ввели не число");
        break;
    }

    if (number) {
        sum += number;
        continue;
    }
}

if (sum) {
    alert("Сумма: " + sum);
}

// Упражнение 3
function getNameOfMonth(m) {
    if (m === 0) return 'январь';
    if (m === 1) return 'февраль';
    if (m === 2) return 'март';
    if (m === 3) return 'апрель';
    if (m === 4) return 'май';
    if (m === 5) return 'июнь';
    if (m === 6) return 'июль';
    if (m === 7) return 'август';
    if (m === 8) return 'сентябрь';
    if (m === 9) return 'октябрь';
    if (m === 10) return 'ноябрь';
    if (m === 11) return 'декабрь';
}
for (let m = 0; m < 12; m++) {
    const month = getNameOfMonth(m);
    if (m === 9) continue;
    console.log(month);
}

// Упражнение 4
// Что такое IIFE?
// IIFE - это JavaScript функция, которая выполняется сразу же после того, как она была определена.
void function () {
    alert("Hello from IIFE!");
}();
